# First discord bot
import os
import discord
from discord.ext import commands


# Settting prefix for commands
bot = commands.Bot(command_prefix='#')


# Info to terminal that bot is ready
@bot.event
async def on_ready():
    print("Bot is Ready!")


@bot.event
async def on_message(message):
    # Ping PONG!!
    if message.content.upper().startswith('!PING'):
        userID = message.author.id
        await bot.send_message(message.channel, "<@{}> Pong!".format(userID))
    # Getting cookie emoticon
    if message.content == "cookie":
        await bot.send_message(message.channel, ":cookie:")
    # Repeating message
    if message.content.startswith('!SAY'):
        args = message.content.split(" ")
        await bot.send_message(message.channel, "{} ".format(" ".join(args[1:])))


#  Getting info about user
@bot.command(pass_context=True)
async def info(ctx, user: discord.Member):
    embed = discord.Embed(title="{}'s info".format(user.name), description="Here is what I could find:", color=0x00ff00)
    embed.add_field(name="Name", value=user.name)
    embed.add_field(name="ID", value=user.id)
    embed.add_field(name="Status", value=user.status)
    embed.add_field(name="Highestrole", value=user.top_role)
    embed.add_field(name="Joined at", value=user.joined_at)
    embed.set_thumbnail(url=user.avatar_url)
    await bot.say(embed=embed)


# Running bot with token
bot.run(os.environ.get('BOT_TOKEN'))
